import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Head from 'next/head';
import _ from 'lodash';

export default function Home() {
  const [imgUrl, setImgUrl] = useState('');
  const [images, setImages] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const onClickSubmit = async () => {
    setIsLoading(true)
  }

  const handleGetImage = async () => {
    let arrayOfImages = []
    try {
        const arrayOfLinks = imgUrl.trim().split('\n')
        for(const link of arrayOfLinks) {
          const { data } = await axios.get(`/api/imgData?url=${link}`, {
            responseType: 'arraybuffer'
          })
          const buffer = Buffer.from(data, 'binary').toString('base64')
          arrayOfImages.push({link,data:'data:image/png;base64, ' + buffer.toString('base64')})
        }
        setImages(arrayOfImages)
    } catch {
        setImages([])
    }
  }

  useEffect(() => {
  if(isLoading) {
    handleGetImage()
  }
  }, [isLoading])

  useEffect(() => {
    if (isLoading) {
      setIsLoading(false)
    }
  }, [images])

  const onChangeUrl = (e) => {
    setImgUrl(e.target.value)
  }

  return (
    <div className='container'>
      <Head>
        <title>Get Identity Card</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <div className='input-container'>
            <button className={`${isLoading && 'disable'}`} disabled={isLoading} onClick={onClickSubmit}> SUBMIT </button>
            <div className='fetch-tip'>One link per line</div>
            <textarea value={imgUrl} name='img-url' onChange={onChangeUrl} />
      </div>
      <div className='img-container'>
        { !_.isEmpty(images) &&
          images.map((image, index) => {
            return <div className='img-link-container' key={index}>
              <div>{image.link}</div>
              <img src={image.data}/>
            </div>
          })
        }
      </div>
    </div>
  )
}
