import axios from 'axios'

export default async (req, res) => {
  try {
    const resImg = await axios.get(req.query.url,
      {
        headers: {
          "User-Agent": process.env.USER_AGENT
        },
        responseType: 'arraybuffer'
      })
      res.statusCode = 200
      res.json(resImg.data)
  } catch {
    res.statusCode = 404
    res.json({})
  }
}
